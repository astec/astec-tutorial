PATH_EMBRYO = '.'

EN = '2019-Tutorial100'

begin = 0
end = 20

DIR_RAWDATA = 'data'
DIR_LEFTCAM_STACKZERO = 'LC/Stack0000'
DIR_RIGHTCAM_STACKZERO = 'RC/Stack0000'
DIR_LEFTCAM_STACKONE = 'LC/Stack0001'
DIR_RIGHTCAM_STACKONE = 'RC/Stack0001'

acquisition_orientation = 'right'
acquisition_mirrors = False
acquisition_resolution = (1., 1., 1.)

target_resolution = 1.0
