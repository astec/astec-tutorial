# Astec Tutorial

This repository contains tutorial data for astec suite (see
[gitlab.inria.fr/astec/astec](https://gitlab.inria.fr/astec/astecl).

Tutorial documentation can be found at [astec.gitlabpages.inria.fr/astec-tutorial/](https://astec.gitlabpages.inria.fr/astec-tutorial/).
