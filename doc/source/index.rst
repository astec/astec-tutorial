============================
Welcome to astec's tutorial!
============================


ASTEC, for **Adaptive Segmentation and Tracking of Embryonic Cells**, was first developed during L. Guignard PhD thesis :cite:p:`guignard:tel-01278725`.

Its development has been pursued, and it was used to process 3D+t movies acquired by the MuViSPIM light-sheet microscope :cite:p:`guignard:hal-02903409`.

.. toctree::
   :maxdepth: 3
   :caption: Contents
   :numbered:
   
   installation.rst
   astec_tutorial.rst
   publications.rst


