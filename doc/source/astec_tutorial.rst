--------------
Astec Tutorial
--------------


Tutorial installation
=====================

Requires git.

Astec tutorial can be found at gitlab.inria.fr/astec/astec-tutorial. It can be downloaded with

.. code-block:: bash
		    
   git clone https://gitlab.inria.fr/astec/astec-tutorial.git

Tutorial data
=============

The directory ``/path/to/astec-tutorial/``, also denoted by
``path/to/experiment/`` or ``<EXPERIMENT>``, contains
the ``RAWDATA/`` and ``parameters/`` sub-directories and a
``README`` file

.. code-block:: none

   /path/to/astec-tutorial/
   ├── data/
   │  ├── LC/
   │  │  └── ...
   │  └── RC/
   │     └── ...
   ├── ...
   ├── parameters
   │  └── ...
   ...
   
The ``data/`` contains 21 time points (indexed from 0 to 20)
of subsampled (for file size
consideration) raw data from a 3D+t movie acquired by a MuViSPIM microscope.

::

   /path/to/experiment/
   ├── data/
   │  ├── LC/
   │  │  ├── Stack0000/
   │  │  │   ├── Time000000_00.mha.gz
   │  │  │   ├── ...
   │  │  │   └── Time000020_00.mha.gz
   │  │  └── Stack0001/
   │  │     ├── Time000000_00.mha.gz
   │  │     ├── ...
   │  │     └── Time000020_00.mha.gz
   │  └── RC/
   │     ├── Stack0000/
   │     │   ├── Time000000_00.mha.gz
   │     │   ├── ...
   │     │   └── Time000020_00.mha.gz
   │     └── Stack0001/
   │        ├── Time000000_00.mha.gz
   │        ├── ...
   │        └── Time000020_00.mha.gz
   ...

where ``LC/`` and ``LC/`` stand respectively for the left
and the right cameras.



Fusion
------

We assume that we are located in the directory
``/path/to/astec-tutorial/``. Running the fusion is
done with |fusion|

.. code-block:: bash
		
  $ astec_fusion -p parameters/1-fuse-tutorial-parameters.py

``1-fuse-tutorial-parameters.py`` being the dedicated parameter file  (listing :numref:`fig-tutorial-parameter-fusion`).

.. code-block:: python
   :linenos:
   :caption: parameter file for the fusion step.
   :name: fig-tutorial-parameter-fusion
		
   PATH_EMBRYO = '.'
   
   EN = '2019-Tutorial100'
   
   begin = 0
   end = 20

   DIR_RAWDATA = 'data'
   DIR_LEFTCAM_STACKZERO = 'LC/Stack0000'
   DIR_RIGHTCAM_STACKZERO = 'RC/Stack0000'
   DIR_LEFTCAM_STACKONE = 'LC/Stack0001'
   DIR_RIGHTCAM_STACKONE = 'RC/Stack0001'
   
   acquisition_orientation = 'right'
   acquisition_mirrors = False
   acquisition_resolution = (1., 1., 1.)
   
   target_resolution = 1.0


* The variable ``PATH_EMBRYO`` is the path to the directory where
  the directory ``data/`` is located. It can be either relative
  (as in the above example) or global (it could have been
  ``/path/to/astec-tutorial/``). 
* The variable ``EN`` is the prefix after which the result fusion
  images will be named. 
* The variables ``begin`` and ``end`` set respectively the
  first and the last index of the input time points to be processed.
* The variable ``DIR_RAWDATA`` is the directory containing the raw
  data.
* The variables ``DIR_LEFTCAM_STACKZERO``,
  ``DIR_RIGHTCAM_STACKZERO``, ``DIR_LEFTCAM_STACKONE`` and
  ``DIR_RIGHTCAM_STACKONE`` set the sub-directories of the
  ``data/`` directory, where the 4 acquisitions
  of the SPIM microscope are located.
* The variables ``acquisition_orientation`` and
  ``acquisition_mirrors`` are parameters
  describing the acquisition geometry.
* The variable ``acquisition_resolution`` is the voxel size
  (along the 3 dimensions X, Y and Z).
* The variable ``target_resolution`` is the desired isotropic (the
  same along the 3 dimensions) voxel size for the result fusion
  images.

After processing, a ``FUSE/`` directory has been created

.. code-block:: none

   /path/to/astec-tutorial/
   ├── FUSE/
   ├── data/
   ├── ...
   ├── parameters
   ...

The ``FUSE/`` directory contains

.. code-block:: none

   FUSE/
   └── FUSE_RELEASE/
      ├── 2019-Tutorial100_fuse_t000.mha
      ├── ...
      ├── 2019-Tutorial100_fuse_t020.mha
      └── LOGS/

The fused images are named after ``<EN>_fuse\t<XXX>.mha``
(where ``<XXX>`` denotes the value of the variable ``XXX``) and
indexed from ``<begin>`` to ``<end>`` (as the input data).

The directory ``LOGS/`` contains
a copy of the parameter file (stamped with date and hour) as well as a
log file (also stamped with date and hour) reporting information about
the processing.


.. _tutorial-intra-registration-fuse:

Sequence intra-registration (or drift/motion compensation) [1]
--------------------------------------------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``. Running the sequence
intra-registration
is done with

.. code-block:: bash

   $ astec_intraregistration -p
   parameters/1.5-intraregistration-tutorial-parameters-fuse.py
   

``1.5-intraregistration-tutorial-parameters-fuse.py`` being the
dedicated parameter file
(listing :numref:`fig-tutorial-parameter-intra-registration-fuse`).

.. code-block:: python
   :linenos:
   :caption: parameter file for the motion compensation step.
   :name: fig-tutorial-parameter-intra-registration-fuse

    PATH_EMBRYO = '.'
    
    EN = '2019-Tutorial100'
    
    begin = 0
    end = 20


* The variable ``PATH_EMBRYO`` is the path to the directory where
  the directory ``FUSE/`` is located. It can be either relative (as in the
  above example) or
  global (it could have been ``/path/to/astec-tutorial/``).
* The variable ``EN`` is the prefix after which the  images
  are named. 
* The variables ``begin`` and ``end`` set respectively the
    first and the last index of the input time points to be processed.


After processing, a ``INTRAREG/`` directory has been created

.. code-block:: none

   /path/to/astec-tutorial/
   ├── FUSE/
   ├── INTRAREG/
   ├── data/
   ├── ...
   ├── parameters
   ...
   

The ``INTRAREG/`` directory contains

.. code-block:: none
		
   INTRAREG/
   └── INTRAREG_RELEASE/
      ├── CO-TRSFS/
      │   ├── 2019-Tutorial100_intrareg_flo000_ref001.trsf
      │   ├── ...
      │   └── 2019-Tutorial100_intrareg_flo019_ref020.trsf
      ├── FUSE/
      │   └── FUSE_RELEASE/
      │      ├── 2019-Tutorial100_intrareg_fuse_t000.mha
      │      ├── ...
      │      └── 2019-Tutorial100_intrareg_fuse_t020.mha
      ├── LOGS/
      ├── MOVIES/
      │   └── FUSE/
      │      └── FUSE_RELEASE/
      │         └── 2019-Tutorial100_intrareg_fuse_t000-020_xy0205.mha
      ├── TRSFS_t0-20/
      │   ├── 2019-Tutorial100_intrareg_t000.trsf
      │   ├── ...
      │   ├── 2019-Tutorial100_intrareg_t020.trsf
      │   └── template_t0-20.mha
      ├── ...
      ├── 2019-Tutorial100_fuse_t020.mha
      └── LOGS/

* The directory ``CO-TRSF/`` contains the co-registration
  transformations.
* The directory ``FUSE/FUSE_RELEASE/`` contains the resampled fused images
  in the same geometry (images have the same dimensions along X, Y and
  Z), with drift compensation (the eventual motion of the sample under the
  microscope has been compensated). 
* The directory ``MOVIES/FUSE/FUSE_RELEASE/`` contains a 3D (which is a 2D+t)
  image, here
  ``2019-Tutorial100_intrareg_fuse_t000-020_xy0205.mha``, which
  the #205 XY-section of the resampled fused images for all the time
  points.
* The directory ``TRSFS/``  contains the transformation of
  every fused image towards the reference one as well as the template
  image (an image large enough to including each fused images after
  resampling).

  The template image ``template_t0-20.mha`` is of size :math:`422
  \times 365 \times 410` with a voxel size of 0.6 (the voxel size can
  be set by the variable ``intra_registration_resolution``).


Segmentation of the first time point
------------------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``. Segmenting the first
time point is
done with

.. code-block:: bash

   $ astec_mars -p parameters/2-mars-tutorial-parameters.py
   

``2-mars-tutorial-parameters.py`` being the
dedicated parameter file (listing :numref:`fig-tutorial-parameter-mars`).

.. code-block:: python
   :linenos:
   :caption: parameter file for the segmentation of the first time point.
   :name: fig-tutorial-parameter-mars

   PATH_EMBRYO = '.'
   	
   EN = '2019-Tutorial100'
   
   begin = 0

* The variable ``PATH_EMBRYO`` is the path to the directory where
  the directory ``FUSE/`` is located. It can be either relative (as in the
  above example) or
  global (it could have been ``/path/to/astec-tutorial/``).
* The variable ``EN`` is the prefix after which the  images
  are named. 
* The variable ``begin`` sets  the
  index of the first input time point (to be processed).

After processing, a ``SEG/`` directory has been created

.. code-block:: none

   /path/to/astec-tutorial/
   ├── FUSE/
   ├── INTRAREG/
   ├── SEG/
   ├── data/
   ├── ...
   ├── parameters
   ...

The ``SEG/`` directory contains

.. code-block:: none

   SEG/
   └── SEG_RELEASE/
      ├── 2019-Tutorial100_mars_t000.mha
      └── LOGS/

``2019-Tutorial100_mars_t000.mha`` is the segmented first
  time point of the sequence.



Correction of  the first time point segmentation
------------------------------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``. Correcting the first
time point segmentation is
done with

.. code-block:: bash

   $ astec_manualcorrection -p parameters/3-manualcorrection-tutorial-parameters.py

``3-manualcorrection-tutorial-parameters.py`` being the
dedicated parameter file
(listing :numref:`fig-tutorial-parameter-manual-correction`).

.. code-block:: python
   :linenos:
   :caption: parameter file for the segmentation correction of the first time point.
   :name: fig-tutorial-parameter-manual-correction

    PATH_EMBRYO = '.'
    
    EN = '2019-Tutorial100'
    
    begin = 0

    mancor_mapping_file='parameters/3-manualcorrection-tutorial.txt'

    
.. code-block:: none
   :linenos:
   :caption: parameter file for the segmentation correction of the first time point.
   :name: fig-tutorial-parameter-manual-correction-txt

   4 9
   10 6
   20 13
   23 30
   12 21
   40 29
   41 37
   51 42
   45 44
   57 61
   59 62
   65 66
   65 74
   78 67
   72 76
   77 82
   83 75

* The variable ``PATH_EMBRYO`` is the path to the directory where
  the directory ``SEG/`` is located. It can be either relative (as in the
  above example) or
  global (it could have been ``/path/to/astec-tutorial/``).
* The variable ``EN`` is the prefix after which the  images
  are named. 
* The variable ``begin`` set  the
  index of the first input time point (to be processed).
* The variable ``mancor_mapping_file`` gives the file
  name containing the correction to be applied.

After processing, the ``SEG/`` directory contains

.. code-block:: none

   SEG/
   └── SEG_RELEASE/
      ├── 2019-Tutorial100_mars_t000.mha
      ├── 2019-Tutorial100_seg_t000.mha
      ├── LOGS/
      └── RECONSTRUCTION/

``2019-Tutorial100_seg_t000.mha`` is the corrected version of
the segmentation obtained at the previous step.



.. _tutorial-segmentation-propagation:

Segmentation propagation
------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``. Segmenting the first
time point is
done with

.. code-block:: bash

   $ astec_astec -p parameters/4-astec-tutorial-parameters.py

``4-astec-tutorial-parameters.py`` being the
dedicated parameter file
(listing :numref:`fig-tutorial-parameter-astec`).

.. code-block:: python
   :linenos:
   :caption: parameter file for the segmentation propagation.
   :name: fig-tutorial-parameter-astec

    PATH_EMBRYO = '.'
    
    EN = '2019-Tutorial100'
    
    begin = 0
    end = 20

    result_lineage_suffix = 'pkl'

After processing, the ``SEG/`` directory contains

.. code-block:: none

   SEG/
   └── SEG_RELEASE/
      ├── 2019-Tutorial100_mars_t000.mha
      ├── 2019-Tutorial100_seg_lineage.pkl
      ├── 2019-Tutorial100_seg_t000.mha
      ├── 2019-Tutorial100_seg_t001.mha
      ├── ...
      ├── 2019-Tutorial100_seg_t020.mha
      ├── LOGS/
      └── RECONSTRUCTION/

``2019-Tutorial100_seg_lineage.pkl`` is a pickle python file
containing a dictionary (in the python sense). It can be read by

.. code-block::

  $ python 
  ...
  >>> import pickle as pkl
  >>> f = open('2019-Tutorial100_seg_lineage.pkl', 'r')
  >>> d = pkl.load(f)
  >>> f.close()
  >>> d.keys()
  ['cell_lineage', 'cell_volume']

  
In this pickle file, cells have an unique identifier :math:`i * 1000 +
c`, which is made of both the image index :math:`i` and the cell
identifier :math:`c` within a segmentation image (recall that, within an image, cells are numbered from 2, 1 being the background
label).



.. _tutorial-intra-registration-seg:

Sequence intra-registration (or drift/motion compensation) [2]
--------------------------------------------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``. Running the sequence intra-registration is
done with

.. code-block:: bash

  $ astec_intraregistration -p parameters/1.5-intraregistration-tutorial-parameters-seg.py 

``1.5-intraregistration-tutorial-parameters-seg.py`` being the
dedicated parameter file
(listing :numref:`fig-tutorial-parameter-intra-registration-seg`).

.. code-block:: python
   :linenos:
   :caption: parameter file for the motion compensation step, segmentation images being used to build the template.
   :name: fig-tutorial-parameter-intra-registration-seg

    PATH_EMBRYO = '.'
    
    EN = '2019-Tutorial100'
    
    begin = 0
    end = 20

    EXP_INTRAREG = 'SEG'
    
    intra_registration_template_type = "SEGMENTATION"
    intra_registration_template_threshold = 2
    intra_registration_margin = 20
    
    intra_registration_resample_segmentation_images = True
    intra_registration_movie_segmentation_images = True


* The variable ``PATH_EMBRYO`` is the path to the directory where
  the directory ``FUSE/`` is located. It can be either relative (as in the
  above example) or
  global (it could have been ``/path/to/astec-tutorial/``).
* The variable ``EN`` is the prefix after which the  images
  are named. 
* The variables ``begin`` and ``end`` set respectively the
  first and the last index of the input time points to be processed.
* the variable ``EXP_INTRAREG`` set the suffix of the
  sub-directory of the ``INTRAREG/`` directory to be created.
* the variable ``intra_registration_template_type`` set
  the images to be used to build the template. Here, since it is
  equal to ``'SEGMENTATION'``, they are the
  segmentation images obtained at the previous step.

  The variable ``intra_registration_template_threshold`` set
  a threshold to be applied to the template images to define the
  information to be kept: we want all the points with a value equal
  or greater than 2 to be contained in the template after
  resampling. Since cells are labeled from 2 and above, the template
  is designed to contain all labeled cells after resampling, so it
  is built as small as possible.

  The variable ``intra_registration_margin`` allows to add
  margins (in the 3 dimensions) to the built template.
  
* The variable
  ``intra_registration_resample_segmentation_images``
  indicates whether the segmentation images are to be resampled in
  the template geometry. 
* The variable
  ``intra_registration_movie_segmentation_images``
  indicates whether 2D+t movies have to be built from the resampled
  segmentation images.


After processing, a ``INTRAREG/INTRAREG_SEG/`` directory has
been created and the ``INTRAREG/`` directory now contains

.. code-block:: none
		
   INTRAREG/
   ├── INTRAREG_RELEASE/
   └── INTRAREG_SEG/
      ├── CO-TRSFS/
      │   ├── 2019-Tutorial100_intrareg_flo000_ref001.trsf
      │   ├── ...
      │   └── 2019-Tutorial100_intrareg_flo019_ref020.trsf
      ├── FUSE/
      │   └── FUSE_RELEASE/
      │      ├── 2019-Tutorial100_intrareg_fuse_t000.mha
      │      ├── ...
      │      └── 2019-Tutorial100_intrareg_fuse_t020.mha
      ├── LOGS/
      ├── MOVIES/
      │   ├── FUSE/
      │   │  └── FUSE_RELEASE/
      │   │     └── 2019-Tutorial100_intrareg_fuse_t000-020_xy174.mha
      │   └── SEG/
      │      └── SEG_RELEASE/
      │         └── 2019-Tutorial100_intrareg_seg_t000-020_xy174.mha
      ├── SEG/
      │   └── SEG_RELEASE/
      │      ├── 2019-Tutorial100_intrareg_seg_t000.mha
      │      ├── ...
      │      └── 2019-Tutorial100_intrareg_seg_t020.mha
      ├── TRSFS_t0-20/
      │   ├── 2019-Tutorial100_intrareg_t000.trsf
      │   ├── ...
      │   ├── 2019-Tutorial100_intrareg_t020.trsf
      │   └── template_t0-20.mha
      ├── ...
      └── LOGS/

In addition to directories already described in section
:ref:`tutorial-intra-registration-fuse`, the ``INTRAREG_SEG/`` directory contains

* The directory ``SEG/SEG_RELEASE`` contains the resampled segmentation images
  in the same geometry (images have the same dimensions along X, Y and
  Z), with drift compensation (the eventual motion of the sample under the
  microscope has been compensated). 
* In addition to a 2D+t movie made from the resampled fusion
  images, the directory ``MOVIES/`` contains a 2D+t movie made from
  the resampled segmentation
  images in the sub-directory ``SEG/SEG_RELEASE``.
* The template image ``template_t0-20.mha`` in the directory
  ``TRSFS/`` 
  is now of size :math:`323 \times 265 \times 348` with a voxel size of 0.6,
  which is smaller than the one computed in section
  :ref:`tutorial-intra-registration-fuse`, even with the added
  margins.
  
  Note that all resampled images (in both the ``FUSE/FUSE_RELEASE`` and the
  ``SEG/SEG_RELEASE`` directories have the same geometry than the template
  image. 



.. _tutorial-properties-seg:

Sequence properties computation [1]
-----------------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``.
Computing cell properties as well as lineage assumes that
segmentation or post-corrected segmentation (see section
:ref:`tutorial-properties-post`) images have been
co-registered (see sections :ref:`tutorial-intra-registration-seg`
and :ref:`tutorial-intra-registration-post`). 
Extracting the sequence properties from the co-registered segmentation
images is
done with

.. code-block:: bash
		
  $ astec_embryoproperties -p parameters/X-embryoproperties-tutorial-parameters-seg.py

``X-embryoproperties-tutorial-parameters-seg.py`` being the
dedicated parameter file
(listing :numref:`fig-tutorial-parameter-properties-seg`).

.. code-block:: python
   :linenos:
   :caption: parameter file for sequence properties computation from the co-registered segmentation images.
   :name: fig-tutorial-parameter-properties-seg

    PATH_EMBRYO = '.'
    
    EN = '2019-Tutorial100'
    
    begin = 0
    end = 20

    EXP_INTRAREG = 'SEG'

* The variable ``PATH_EMBRYO`` is the path to the directory where
  the directory ``FUSE/`` is located. It can be either relative (as in the
  above example) or
  global (it could have been ``/path/to/astec-tutorial/``).
* The variable ``EN`` is the prefix after which the  images
  are named. 
* The variables ``begin`` and ``end`` set respectively the
  first and the last index of the input time points to be processed.
* the variable ``EXP_INTRAREG`` set the suffix of the
  sub-directory of the ``INTRAREG/`` directory where to search
  post-corrected segmentation or segmentation images.

  Since the directory ``INTRAREG/INTRAREG_SEG/``  only
  contains the co-registered segmentation images (in the
  ``SEG/SEG_RELEASE/`` sub-directory), properties will be computed from
  these images.

After processing, some files appears in the ``INTRAREG/INTRAREG_SEG/SEG/SEG_RELEASE/`` sub-directory 

.. code-block:: none
		
   INTRAREG/
   ├── INTRAREG_RELEASE/
   └── INTRAREG_SEG/
      ├── CO-TRSFS/
      │   └── ...
      ├── FUSE/
      │   └── ...
      ├── LOGS/
      ├── MOVIES/
      │   └── ...
      ├── SEG/
      │   └── SEG_RELEASE/
      │      ├── 2019-Tutorial100_intrareg_seg_lineage.pkl
      │      ├── 2019-Tutorial100_intrareg_seg_lineage.tlp
      │      ├── 2019-Tutorial100_intrareg_seg_lineage.txt
      │      ├── 2019-Tutorial100_intrareg_seg_lineage.xml
      │      ├── 2019-Tutorial100_intrareg_seg_t000.mha
      │      ├── ...
      │      └── 2019-Tutorial100_intrareg_seg_t020.mha
      ├── TRSFS_t0-20/
      │   └── ...
      ├── ...
      └── LOGS/

``2019-Tutorial100_intrareg_seg_lineage.pkl`` is a pickle python file
containing a dictionary (in the python sense). It can be read by

.. code-block::

  $ python 
  ...
  >>> import pickle as pkl
  >>> f=open('2019-Tutorial100_intrareg_seg_lineage.pkl', 'r')
  >>> d = pkl.load(f)
  >>> f.close()
  >>> d.keys()
  ['all_cells', 'cell_barycenter', 'cell_contact_surface', 'cell_principal_vectors', 'cell_principal_values', 'cell_volume', 'cell_compactness', 'cell_surface', 'cell_lineage']


In this pickle file (as in the one computed at section
:ref:`tutorial-segmentation-propagation`), cells have an unique
identifier :math:`i * 1000 + c`, which is made of 
both the image index :math:`i` and the cell identifier :math:`c` within a segmentation
image (recall that cells are numbered from 2, 1 being the background
label).

``2019-Tutorial100_intrareg_seg_lineage.xml`` contains the
same information than the pickle file, but in xml format (see figure
\ref{fig:tutorial:seg:properties:xml}).

.. code-block:: none
   :caption: XML output properties file from the co-registered segmentation images
   :name: fig-tutorial-seg-properties-xml
	  
   <data>
     <cell_volume>
       ...
     </cell_volume>
     <cell_surface>
        ...
     </cell_surface>
     <cell_compactness>
        ...
     </cell_compactness>
     <cell_barycenter>
        ...
     </cell_barycenter>
     <cell_principal_values>
        ...
     </cell_principal_values>
     <cell_principal_vectors>
        ...
     </cell_principal_vectors>
     <cell_contact_surface>
        ...
     </cell_contact_surface>
     <all_cells>[2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 
        ...
        200097, 200099, 200100, 200101, 200102]</all_cells>
     <cell_lineage>
        ...
     </cell_lineage>
   </data>

``2019-Tutorial100_intrareg_seg_lineage.tst`` contains some
*diagnosis* information (smallest and largest cells, weird
lineages, etc.). 



.. _tutorial-post-segmentation:

Segmentation post-correction
----------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``. Segmentation post-correction is
done with

.. code-block:: bash
		
  $ astec_postcorrection -p parameters/5-postcorrection-tutorial-parameters.py 

``5-postcorrection-tutorial-parameters.py`` being the
dedicated parameter file  (figure :ref:`fig-tutorial-parameter-post-correction`).

.. code-block:: python
   :linenos:
   :caption: parameter file for the segmentation post-correction.
   :name: fig-tutorial-parameter-post-correction

    PATH_EMBRYO = '.'
    
    EN = '2019-Tutorial100'
    
    begin = 0
    end = 20

    result_lineage_suffix = 'pkl'
    

After processing, a ``POST/`` directory has been created


.. code-block:: none

   /path/to/astec-tutorial/
   ├── FUSE/
   ├── INTRAREG/
   ├── SEG/
   ├── POST/
   ├── data/
   ├── ...
   ├── parameters
   ...


The ``POST/`` directory contains

.. code-block:: none

   POST/
   └── POST_RELEASE/
      ├── 2019-Tutorial100_post_lineage.pkl
      ├── 2019-Tutorial100_post_t000.mha
      ├── ...
      ├── 2019-Tutorial100_post_t020.mha
      └── LOGS/


      
.. _tutorial-intra-registration-post:

Sequence intra-registration (or drift compensation) [3]
-------------------------------------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``. Running the sequence intra-registration is
done with

.. code-block:: bash
		
  $ astec_intraregistration -p parameters/1.5-intraregistration-tutorial-parameters-post.py 

``1.5-intraregistration-tutorial-parameters-post.py`` being the
dedicated parameter file  (figure :ref:`fig-tutorial-parameter-intra-registration-post`).

.. code-block:: python
   :linenos:
   :caption: parameter file for the motion compensation step, post-segmentation images being used to build the template.
   :name: fig-tutorial-parameter-intra-registration-post

    PATH_EMBRYO = '.'
    
    EN = '2019-Tutorial100'
    
    begin = 0
    end = 20

    EXP_INTRAREG = 'SEG'
    
    intra_registration_template_type = "POST-SEGMENTATION"
    intra_registration_template_threshold = 2
    intra_registration_margin = 20
    
    intra_registration_resample_segmentation_images = True
    intra_registration_movie_segmentation_images = True
    intra_registration_movie_post_segmentation_images = True
    intra_registration_movie_segmentation_images = True


* The variable ``PATH_EMBRYO`` is the path to the directory where
  the directory ``FUSE/`` is located. It can be either relative (as in the
  above example) or
  global (it could have been ``/path/to/astec-tutorial/``).
* The variable ``EN`` is the prefix after which the  images
  are named. 
* The variables ``begin`` and ``end`` set respectively the
  first and the last index of the input time points to be processed.
* the variable ``EXP_INTRAREG`` set the suffix of the
  sub-directory of the ``INTRAREG/`` directory to be created.
* the variable ``intra_registration_template_type`` set
  the images to be used to build the template. Here, since it is
  equal to ``"POST-SEGMENTATION''``, they are the
  post-corrected segmentation images obtained at the previous step.

  The variable ``intra_registration_template_threshold`` set
  a threshold to be applied to the template images to define the
  information to be kept: we want all the points with a value equal
  or greater than 2 to be contained in the template after
  resampling. Since cells are labeled from 2 and above, the template
  is designed to contain all labeled cells after resampling, so it
  is built as small as possible.

  The variable ``intra_registration_margin`` allows to add
  margins (in the 3 dimensions) to the built template.
  
* The variable
  ``intra_registration_resample_post_segmentation_images``
  indicates whether the post-corrected segmentation images are to be resampled in
  the template geometry. 
* The variable
  ``intra_registration_resample_segmentation_images``
  indicates whether the segmentation images are to be resampled in
  the template geometry. 
* The variable
  ``intra_registration_movie_post_segmentation_images``
  indicates whether 2D+t movies have to be built from the resampled
  post-corrected segmentation images.
* The variable
  ``intra_registration_movie_segmentation_images``
  indicates whether 2D+t movies have to be built from the resampled
  segmentation images.


After processing, a ``INTRAREG/INTRAREG_POST/`` directory has
been created and the ``INTRAREG/`` directory now contains

.. code-block:: none
		
   INTRAREG/
   ├── INTRAREG_RELEASE/
   ├── INTRAREG_POST/
   │  ├── CO-TRSFS/
   │  │   ├── 2019-Tutorial100_intrareg_flo000_ref001.trsf
   │  │   ├── ...
   │  │   └── 2019-Tutorial100_intrareg_flo019_ref020.trsf
   │  ├── FUSE/
   │  │   └── FUSE_RELEASE/
   │  │      ├── 2019-Tutorial100_intrareg_fuse_t000.mha
   │  │      ├── ...
   │  │      └── 2019-Tutorial100_intrareg_fuse_t020.mha
   │  ├── LOGS/
   │  ├── MOVIES/
   │  │   ├── FUSE/
   │  │   │  └── FUSE_RELEASE/
   │  │   │     └── 2019-Tutorial100_intrareg_fuse_t000-020_xy174.mha
   │  │   ├── POST/
   │  │   │  └── POST_RELEASE/
   │  │   │     └── 2019-Tutorial100_intrareg_post_t000-020_xy174.mha
   │  │   └── SEG/
   │  │      └── SEG_RELEASE/
   │  │         └── 2019-Tutorial100_intrareg_seg_t000-020_xy174.mha
   │  ├── POST/
   │  │   └── POST_RELEASE/
   │  │      ├── 2019-Tutorial100_intrareg_post_t000.mha
   │  │      ├── ...
   │  │      └── 2019-Tutorial100_intrareg_pst_t020.mha
   │  ├── SEG/
   │  │   └── SEG_RELEASE/
   │  │      ├── 2019-Tutorial100_intrareg_seg_t000.mha
   │  │      ├── ...
   │  │      └── 2019-Tutorial100_intrareg_seg_t020.mha
   │  ├── TRSFS_t0-20/
   │  │   ├── 2019-Tutorial100_intrareg_t000.trsf
   │  │   ├── ...
   │  │   ├── 2019-Tutorial100_intrareg_t020.trsf
   │  │   └── template_t0-20.mha
   │  ├── ...
   │  └── LOGS/
   ├── INTRAREG_RELEASE/
   │   └── ...
   └── INTRAREG_SEG/
       └── ...


In addition to directories already described in sections
:ref:`tutorial-intra-registration-fuse` and
:ref:`tutorial-intra-registration-seg`,
the ``INTRAREG_POST/`` directory contains

* The directory ``POST/POST_RELEASE/`` contains the resampled
  post-corrected segmentation images
  in the same geometry (images have the same dimensions along X, Y and
  Z), with drift compensation (the eventual motion of the sample under the
  microscope has been compensated). 
* In addition to a 2D+t movie made from the resampled fusion
  and the segmentation images, the directory ``MOVIES/`` contains
  a 2D+t movie made from the resampled post-corrected segmentation
  images in the sub-directory ``POST/POST_RELEASE/``.
* The template image ``template_t0-20.mha`` in the directory
  ``TRSFS/`` 
  is of size :math:`323 \times 265 \times 348` with a voxel size of 0.6,
  has the same size than the one computed in section
  :ref:`tutorial-intra-registration-seg`,  which is expected since
  the post-correction does not change the background.
  Note that all resampled images (in the ``FUSE/FUSE_RELEASE/``, the
  ``POST/POST_RELEASE/``, and the
  ``SEG/SEG_RELEASE/`` directories have the same geometry than the template
  image. 


  
.. _tutorial-properties-post:

Sequence properties computation [2]
-----------------------------------

We assume that we are located in the directory
``/path/to/astec-tutorial/``.
Computing cell properties as well as lineage assumes that
segmentation or post-corrected segmentation (see section
:ref:`tutorial-post-segmentation`}) images have been
co-registered (see sections :ref:`tutorial-intra-registration-fuse` and
:ref:`tutorial-intra-registration-seg`). 
Extracting the sequence properties from the co-registered segmentation
images is
done with

.. code-block:: bash

  $ astec_embryoproperties -p parameters/X-embryoproperties-tutorial-parameters-post.py


``X-embryoproperties-tutorial-parameters-post.py`` being the
dedicated parameter file
(listing :numref:`fig-tutorial-parameter-properties-post`).

.. code-block:: python
   :linenos:
   :caption: parameter file for sequence properties computation from the co-registered post-segmentation images.
   :name: fig-tutorial-parameter-properties-post

    PATH_EMBRYO = '.'
    
    EN = '2019-Tutorial100'
    
    begin = 0
    end = 20

    EXP_INTRAREG = 'POST'

* The variable ``PATH_EMBRYO`` is the path to the directory where
  the directory ``FUSE/`` is located. It can be either relative (as in the
  above example) or
  global (it could have been ``/path/to/astec-tutorial/``).
* The variable ``EN`` is the prefix after which the  images
  are named. 
* The variables ``begin`` and ``end`` set respectively the
  first and the last index of the input time points to be processed.
* the variable ``EXP_INTRAREG`` set the suffix of the
  sub-directory of the ``INTRAREG/`` directory where to search
  post-corrected segmentation or segmentation images.

  Since the directory ``INTRAREG/INTRAREG_POST/`` 
  contains the co-registered post-corrected segmentation images (in the
  ``POST/POST_RELEASE/`` sub-directory), properties will be computed from
  these images preferably to the co-registered segmentation images (in the
  ``SEG/SEG_RELEASE/`` sub-directory).
  
After processing, some files appears in the ``INTRAREG/INTRAREG_POST/POST/`` sub-directory 

.. code-block:: none
		
   INTRAREG/
   ├── INTRAREG_POST/
   │  ├── CO-TRSFS/
   │  │   └── ...
   │  ├── FUSE/
   │  │   └── ...
   │  ├── LOGS/
   │  ├── MOVIES/
   │  │   └── ...
   │  ├── POST/
   │  │   └── POST_RELEASE/
   │  │      ├── 2019-Tutorial100_intrareg_post_lineage.pkl
   │  │      ├── 2019-Tutorial100_intrareg_post_lineage.tlp
   │  │      ├── 2019-Tutorial100_intrareg_post_lineage.txt
   │  │      ├── 2019-Tutorial100_intrareg_post_lineage.xml
   │  │      ├── 2019-Tutorial100_intrareg_post_t000.mha
   │  │      ├── ...
   │  │      └── 2019-Tutorial100_intrareg_post_t020.mha
   │  ├── SEG/   
   │  ├── TRSFS_t0-20/
   │  │   └── ...
   │  ├── ...
   │  └── LOGS/
   ├── INTRAREG_RELEASE/
   │   └── ...
   └── INTRAREG_SEG/
       └── ...
   

Those files have the same content than the ones already presented in
section :ref:`tutorial-properties-seg`.
